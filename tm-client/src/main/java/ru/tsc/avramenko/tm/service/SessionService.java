package ru.tsc.avramenko.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.service.ISessionService;
import ru.tsc.avramenko.tm.endpoint.Session;

@Getter
@Setter
public class SessionService implements ISessionService {

    @Nullable
    private Session session;

}