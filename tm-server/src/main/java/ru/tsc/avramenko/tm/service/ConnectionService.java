package ru.tsc.avramenko.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.service.IConnectionService;
import ru.tsc.avramenko.tm.api.service.IPropertyService;
import ru.tsc.avramenko.tm.exception.system.ProcessException;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public Connection getConnection() {
        @Nullable final String username = propertyService.getJdbcUser();
        if (username == null) throw new ProcessException();
        @Nullable final String password = propertyService.getJdbcPass();
        if (password == null) throw new ProcessException();
        @Nullable final String url = propertyService.getJdbcUrl();
        if (url == null) throw new ProcessException();
        @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }

}