package ru.tsc.avramenko.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.User;

public interface ISessionRepository extends IRepository<Session> {

    boolean contains(@NotNull String id);

    @Nullable
    @SneakyThrows
    Session add(@Nullable Session session);

    @Nullable
    @SneakyThrows
    Session update(@Nullable Session entity);

}